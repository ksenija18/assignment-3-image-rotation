#ifndef FILE_H
#define FILE_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>


FILE *open_file_for_reading(const char *filename);

FILE *open_file_for_writing(const char *filename);

struct image read_image(const char *filename);

void write_image(const char *filename, const struct image *img);

void close_file(FILE *file);

#endif
