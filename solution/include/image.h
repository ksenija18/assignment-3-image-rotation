
#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>


struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};


struct pixel {
    uint8_t b, g, r;
};


struct image creation(uint64_t w, uint64_t h);

void removal(struct image *img);


#endif
