#ifndef PARSING_H
#define PARSING_H

#include "image.h"
#include "status.h"
#include <stdint.h>
#include <stdio.h>



#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


int read_bmp_data(FILE *in, struct image *img, const struct bmp_header *header, size_t padding);

enum read_status read_bmp_header(FILE *in, struct bmp_header *header);

enum write_status write_image_data(FILE *out, const struct image *img);

enum read_status parse_from_bmp(FILE *in, struct image *img);

enum write_status parse_to_bmp(FILE *out, const struct image *img);


#endif

