#ifndef STATUS_H
#define STATUS_H


enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER = 1,
    READ_INVALID_BITS = 2

};

enum write_status {
    WRITE_OK = 0,
    WRITE_IMAGE_ERROR = 1,
    WRITE_HEADER_ERROR = 2,
    WRITE_MEMORY_ERROR
};

#endif
