
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stdint.h>

#define PIXEL_SIZE sizeof(struct pixel)
#define PADDING_ALIGNMENT 4
#define BMP_HEADER_TYPE 0x4D42
#define BMP_HEADER_SIZE 40
#define BI_BIT_COUNT 24
#define BMP_RESERVED 0
#define BMP_PLANES 1
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 0
#define BMP_Y_PELS_PER_METER 0
#define BMP_CLR_IMPORTANT 0
#define BMP_CLR_USED 0

#endif
