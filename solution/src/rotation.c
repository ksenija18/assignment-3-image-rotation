#include "../include/image.h"
#include "../include/rotation.h"
#include <stdio.h>
#include <stdlib.h>



#define ONE_ROTATION_IN_DEGREE 90
#define FULL_ROTATION_IN_DEGREE 360

struct image rotate90(const struct image source) {
    struct image result = creation(source.height, source.width);
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            result.data[(result.height - 1 - j) * result.width + i] = source.data[i * source.width + j];
        }
    }
    return result;
}

struct image rotate180(const struct image source) {
    struct image result;
    result.width = source.width;
    result.height = source.height;
    result.data = (struct pixel *)malloc(result.width * result.height * sizeof(struct pixel));
    if (result.data == NULL) {
        perror("Error allocating memory for rotated image");
        return source;
    }

    for (uint64_t i = 0; i < result.width * result.height; i++) {
        result.data[i] = source.data[(source.height - 1 - i / result.width) * result.width +
                                     (result.width - 1 - i % result.width)];
    }
    return result;
}

struct image rotate270(const struct image source) {
    struct image result = creation(source.height, source.width);
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            result.data[j * result.width + (result.width - 1 - i)] = source.data[i * source.width + j];
        }
    }
    return result;
}

struct image rotation(const struct image source, int angle) {
    struct image result;

    if (!((angle + FULL_ROTATION_IN_DEGREE) % ONE_ROTATION_IN_DEGREE == 0 && angle >= -270 && angle <= 270)) {
        perror("Invalid angle. Rotation angle must be one of: 0, 90, -90, 180, -180, 270, -270\n");
        return source;
    }

    result.width = source.width;
    result.height = source.height;

    result.data = (struct pixel *) malloc(result.width * result.height * sizeof(struct pixel));

    if (result.data == NULL) {
        perror("Error allocating memory for rotated image");
        return source;
    }

    switch (angle) {
        case 0:
            for (uint64_t i = 0; i < result.width * result.height; i++) {
                result.data[i] = source.data[i];
            }
            break;
        case 90:
        case -270:
            free(result.data);
            result = rotate90(source);
            break;
        case 180:
        case -180:
            free(result.data);
            result = rotate180(source);
            break;
        case -90:
        case 270:
            free(result.data);
            result = rotate270(source);
            break;
        default:
            break;
    }

    return result;
}
