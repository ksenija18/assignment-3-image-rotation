#include "../include/image.h"
#include "../include/file.h"
#include "../include/rotation.h"
#include <stdio.h>
#include <stdlib.h>


void print_usage(void) {
    fprintf(stderr, "Wrong arguments, it should be: [source file] [resulting file] [angle]");
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        print_usage();
        return 1;
    }

    int angle;
    char *endptr;

    angle = (int) strtol(argv[3], &endptr, 10);

    if (*endptr != '\0' && *endptr != '\n') {
        fprintf(stderr, "Invalid angle value");
        return 1;
    }

    struct image img = read_image(argv[1]);
    if (img.data == NULL) {
        fprintf(stderr, "Failed to read the source image");
        return 1;
    }

    struct image result = rotation(img, angle);
    removal(&img);
    if (result.data == NULL) {
        fprintf(stderr, "Failed to rotate the image");
        removal(&result);
        return 1;
    }
    write_image(argv[2], &result);
    removal(&result);


    return 0;
}
