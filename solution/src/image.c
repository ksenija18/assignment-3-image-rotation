#include "image.h"
#include <stdio.h>
#include <stdlib.h>



struct image creation(uint64_t w, uint64_t h) {
    struct image img;
    img.width = w;
    img.height = h;

    if ((img.data = malloc(w * h * sizeof(struct pixel))) == NULL) {
        perror("There was an error with memory allocation");
        exit(EXIT_FAILURE);
    }

    return img;
}


void removal(struct image *img) {
    if (img && img->data) {
        free(img->data);
    }
}
