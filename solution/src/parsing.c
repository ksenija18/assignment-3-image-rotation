#include "../include/constants.h"
#include "../include/file.h"
#include "../include/parsing.h"
#include <stdint.h>
#include <stdlib.h>






static size_t padding(uint32_t width) {
    return (PADDING_ALIGNMENT - (width * PIXEL_SIZE) % PADDING_ALIGNMENT) % PADDING_ALIGNMENT;
}
static void set_padding(uint8_t *start, size_t count) {
    for (size_t i = 0; i < count; i++) {
        start[i] = 0;
    }
}

int read_bmp_data(FILE *in, struct image *img, const struct bmp_header *header, size_t padding) {
    size_t rowSize = header->biWidth * sizeof(struct pixel) + padding;
    uint8_t *row = malloc(rowSize);
    if (!row) {
        removal(img);
        return 0;
    }
    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fread(row, header->biWidth * sizeof(struct pixel) + padding, 1, in) < 1) {
            perror("Error reading image row from file");
            free(row);
            removal(img);
            return 0;
        }
        struct pixel *destination = img->data + i * header->biWidth;
        struct pixel *source = (struct pixel *) row;

        for (size_t j = 0; j < header->biWidth; j++) {
            destination[j] = source[j];
        }

    }
    free(row);
    return 1;
}


enum read_status parse_from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;

    if (read_bmp_header(in, &header) != READ_OK) {
        return READ_INVALID_HEADER;
    }

    struct image newImage = creation(header.biWidth, header.biHeight);
    size_t bmp_padding = padding(header.biWidth);
    *img = newImage;

    if (!read_bmp_data(in, img, &header, bmp_padding)) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status read_bmp_header(FILE *in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) < 1) {
        perror("Error reading BMP header from file");
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}




enum write_status write_image_data(FILE *out, const struct image *img) {
    size_t bmp_padding = padding(img->width);
    size_t rowLength = img->width * sizeof(struct pixel) + bmp_padding;
    uint8_t *row = malloc(rowLength);

    if (row == NULL) {
        perror("Error allocating memory for image row");
        return WRITE_MEMORY_ERROR;
    }


    for (uint32_t i = 0; i < img->height; i++) {
        struct pixel *pixelRow = (struct pixel *) row;
        struct pixel *source = img->data + i * img->width;

        for (size_t j = 0; j < img->width; j++) {
            if (j < img->width) {
                pixelRow[j] = source[j];
            }
        }

        set_padding(row + img->width * sizeof(struct pixel), bmp_padding);


        if (fwrite(row, rowLength, 1, out) < 1) {
            perror("Error writing image row to file");
            free(row);
            return WRITE_IMAGE_ERROR;
        }
    }
    free(row);
    return WRITE_OK;
}


void initialize_bmp_header(struct bmp_header *header, const struct image *img) {
    header->bfType = BMP_HEADER_TYPE;
    header->bfileSize = BMP_HEADER_SIZE + img->width * img->height * PIXEL_SIZE;
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = BMP_HEADER_SIZE;
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = BMP_PLANES;
    header->biBitCount = BI_BIT_COUNT;
    header->biCompression = BMP_COMPRESSION;
    header->biSizeImage = img->width * img->height * PIXEL_SIZE;
    header->biXPelsPerMeter = BMP_X_PELS_PER_METER;
    header->biYPelsPerMeter = BMP_Y_PELS_PER_METER;
    header->biClrUsed = BMP_CLR_USED;
    header->biClrImportant = BMP_CLR_IMPORTANT;
}


enum write_status parse_to_bmp(FILE *out, const struct image *img) {
    if (!out || !img) {
        fprintf(stderr, "Invalid input parameters");
        return WRITE_HEADER_ERROR;
    }

    size_t headerSize = sizeof(struct bmp_header);
    size_t pixelSize = sizeof(struct pixel);
    uint64_t fileSize = headerSize + img->width * img->height * pixelSize;
    uint64_t imageSize = img->width * img->height * pixelSize;


    if (fileSize > UINT32_MAX || imageSize > UINT32_MAX) {
        fprintf(stderr, "The image size is too big");
        return WRITE_HEADER_ERROR;
    }

    struct bmp_header header;
    initialize_bmp_header(&header, img);

    if (fwrite(&header, sizeof(header), 1, out) < 1) {
        perror("Error writing header to file");
        return WRITE_HEADER_ERROR;
    }


    if (write_image_data(out, img) != WRITE_OK) {
        return WRITE_IMAGE_ERROR;
    }

    return WRITE_OK;
}
