#include "../include/parsing.h"
#include "../include/file.h"
#include <stdio.h>
#include <stdlib.h>


FILE *open_file_for_reading(const char *filename) {
    FILE *file = fopen(filename, "rb");
    return file;
}

FILE *open_file_for_writing(const char *filename) {
    FILE *file = fopen(filename, "wb");
    return file;
}


enum read_status parsing_from(FILE *file, struct image *img) {
    return parse_from_bmp(file, img);
}

enum write_status parsing_to(FILE *file, const struct image *img) {
    return parse_to_bmp(file, img);
}


void exit_with_error(FILE *file, const char *error_message) {
    if (file) {
        close_file(file);
    }
    perror(error_message);
    exit(1);
}

void close_file(FILE *file) {
    if (!file) {
        return;
    }
    if (fclose(file) != 0) {
        perror("There was an error when closing the file");
    }
}


struct image read_image(const char *filename) {
    FILE *input = open_file_for_reading(filename);

    if (input == NULL) {
        perror("Error opening file for reading");
        exit(EXIT_FAILURE);
    }

    struct image img;
    if (parsing_from(input, &img) != READ_OK) {
        exit_with_error(input, " There was an error while reading the image from file");
    }

    close_file(input);
    return img;
}

void write_image(const char *filename, const struct image *img) {
    FILE *output = open_file_for_writing(filename);

    if (output == NULL) {
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
    }

    if (parsing_to(output, img) != WRITE_OK) {
        exit_with_error(output, "There was an error while writing the image from file");
    }

    close_file(output);
}


